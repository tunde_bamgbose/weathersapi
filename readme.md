## Weather's API

### Fullstack Developer Test, Xpedigo 

#### How to run
1. Clone repository ```git clone https://bitbucket.org/tunde_bamgbose/weathersapi.git```
2. Update database information in env file
3. Run ```composer install```
4. Run ```php artisan key:generate```
5. Run ```php artisan migrate``` to create database tables
6. Run ```php artisan db:seed``` to run seeders to create dummy records
7. Run ```npm install``` to intsall dependencies
8. Run ```php artisan serve`` access project at localhost:8000 :)